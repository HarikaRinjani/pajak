<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
      <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{url('vendor/crudbooster/assets/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="url('vendor/crudbooster/assets/adminlte/plugins/iCheck/all.css')}}">
    <link rel="stylesheet" href="{{url('css/custom.css')}}">
    <link rel="stylesheet" href="{{url('vendor/crudbooster/assets/adminlte/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Lato" />
    <link rel="stylesheet" href="{{url('vendor/assets/css/hover.css')}}">
    <link href="{{url('assets/css/jquery.toast.min.css')}}" rel="stylesheet">
    <link href="{{url('assets/css/jquery.msgbox.css')}}" rel="stylesheet">
    @stack('css')

    <title>Tanya Pajak</title>
  </head>
  <body>
  @yield('content')
  
  <script src="{{url('vendor/crudbooster/assets/adminlte/plugins/jQuery/jQuery-2.1.4.min.js')}}"></script>
  <script src="{{url('vendor/crudbooster/assets/bootstrap/js/bootstrap.min.js')}}"></script>
  <script src="{{url('vendor/crudbooster/assets/adminlte/plugins/iCheck/icheck.min.js')}}"></script>
  <script src="{{url('vendor/assets/js/flickity-docs.min.js')}}"></script>
  <script src="{{url('vendor/assets/js/smooth-scroll/smooth-scroll.js')}}"></script>
  <script src="{{url('assets/js/jquery.toast.min.js')}}"></script>
  <script src="{{url('assets/js/jquery.msgbox.js')}}"></script>
</script>
@if (\Session::has('message'))
<script type="text/javascript">
  $.toast({
    heading: '{!!  \Session::get('head') !!}',
    text: '{!!  \Session::get('message') !!}',
    position: 'bottom-right',
    loaderBg:'#e8af48',
    icon: '{!!  \Session::get('type') !!}',
    hideAfter: 5000, 
    stack: 6
  });
</script>
@endif
@if (\Session::has('popup'))
<script type="text/javascript">
  $.msgbox({
    type: 'info',
    content: '<p>Email Anda Telah Terkirim</p><br><p>Silahkan Menunggu Balasan Dari Admin</p>',
    title: 'Info'
  });
</script>
@endif
  <script>
  (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
  function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
  e=o.createElement(i);r=o.getElementsByTagName(i)[0];
  e.src='//www.google-analytics.com/analytics.js';
  r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
  ga('create','UA-52746336-1');ga('send','pageview');
  var isCompleted = {};
  function sampleCompleted(sampleName){
    if (ga && !isCompleted.hasOwnProperty(sampleName)) {
      ga('send', 'event', 'WebCentralSample', sampleName, 'completed');
      isCompleted[sampleName] = true;
    }
  }
</script>
  @stack('script')
  </body>
</html>