@extends('tema.master')
@section('content')

<div class="app-container">
  <!-- HEADER -->
 <span id="pajak"></span>
  <header class="head">
    <div class="navbar trasparent">
      <div class="navbar-inner" id="nav-content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-3 col-sm-4 col-12 pajak">
              
              <a href="http://tanyapajak.id" class="logo" id="logo-img">
                <img src="{{url('/vendor/assets/logo_tanya_pajak_id.png')}}"  height="38px" alt="">
              </a>
            </div>
            <div class="col-md-9 col-sm-8 col-12">
              <div class="nav-faq pull-right">
                <a href="#faq" data-easing="easeInQuad" class="faq">FAQ</a>
              </div>
            </div>
          </div>

          <div>
            <hr class="hr-head">
          </div>
        </div>
      </div>
    </div>

    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-12">
            <div class="col-sm-5 font-content">
              <p class=font_1>Bertanya dan konsultasi seputar pajak?</p>
              <p class="font_2">Anda sedang membutuhkan informasi mengenai pajak? Silakan DAFTAR, kami punya konsultan pajak BERLISENSI  yang siap membantu Anda</p>
            </div>
            <div class="col-sm-4">
              <div class="fam-con">
              <iframe class="fam" src="https://tawk.to/chat/5c678dbaf324050cfe33795c/default" scrolling="no">Iframes not supported</iframe>
            </div>
          <!--     <div class="registrasi_card">
                <p class="font_3">Silakan lengkapi form di bawah ini</p>
                <p class="font_4">Jika Anda ingin bertanya dan berkonsultasi seputar pajak dengan kami. Pendaftaran <b>GRATIS!</b></p>
                <form action="">
                  <div class="form-group">
                    <input type="text" name="nama" required="" class="form-control f" placeholder="Nama">
                  </div>
                  <div class="form-group">
                    <input type="number" name="hp" required="" class="form-control f" placeholder="Nomor Handphone">
                  </div>

                  <p class="font_5">
                    *Pastikan Anda memasukkan nomor handphone yang aktif dan bisa Kami hubungi
                  </p>

                  <div class="form-group">
                    <input type="email" name="email" required="" class="form-control f" placeholder="Email">
                  </div>

                  <p class="font_5">
                    *Pastikan Anda memasukkan email yang aktif
                  </p>

                  <p class="font_6">Tipe Konsultasi</p>

                  <div class="form-group">
                    <div class="row">
                      <div class="col-sm-6 col-6">

                        <input type="radio" name="konsultan" value="1" class="flat-red"><span class="cek">&nbsp;konsultasi Pribadi</span>

                      </div>

                      <div class="col-sm-6 col-6">

                        <input type="radio" name="konsultan" value="2" class="flat-red"><span class="cek">&nbsp;Badan Usaha</span>

                      </div>
                    </div>
                  </div>

                  <div class="box-footer">
                    <button type="submit" class="btn btn-warning submit-reg">DAFTAR</button>
                  </div>

                </form>
              </div> -->
            </div>
          </div>
        </div>
      </div>
    </div>

  </header>
</div>

<div class="section_2">
  <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-sm-12">
       <img src="{{url('vendor/assets/landing/icon/ic_sparation.png')}}" class="center-img"  width="auto" height="auto" alt="">
     </div>
     <div class="col-sm-12">
      <p class="font-head1">Kami hadir untuk membantu, dan apa saja yang menjadikan kami <br> pantas menjadi mitra Anda?</p>
     </div>
   </div>
   <div class="container"> 
    <div class="row justify-content-center">

     <div class="col-md-12">
       <img src="{{url('assets/landing/landing/icon/ic_line.png')}}" class="section-line center-img"  width="auto" height="auto" alt="">
     </div>
     <div class="row cardd">
      <div class="col-md-4 cd">
        <div class="card" id="img1">
         <img src="{{url('vendor/assets/landing/icon/ic_law@2x.png')}}" id="g1" id class="img-card"  width="107px" height="auto" alt="">  
         <p class="heading-font" id="f1">Terhindar Sanksi Pajak</p> 
         <p class="content-font" id="f11">Anda akan merasa tenang karena terhindar dari sanksi pajak</p> 
       </div>
     </div>
     <div class="col-md-4 cd">
      <div class="card" id="img2"> 
       <img src="{{url('vendor/assets/landing/icon/ic_tax_audit@2x.png')}}" id="g2" id class="img-card"  width="107px" height="auto" alt="">

       <p class="heading-font" id="f2">Up-to-date</p>
       <p class="content-font" id="f22">Tidak masalah ada peraturan baru tiap tahunnya Anda akan merasa tenang dibantu konsultan Kami</p>
     </div>
   </div>
   <div class="col-md-4 cd">
    <div class="card" id="img3">  
     <img src="{{url('vendor/assets/landing/icon/ic_licence@2x.png')}}" id="g3" id class="img-card"  width="107px" height="auto" alt="">
     <p class="heading-font" id="f3" style="margin-top:35px!important ">Berlisensi dan Berpengalaman</p>
     <p class="content-font" id="f33">Anda wajib tau bahwa konsultan kami sudah berlisensi dan berpengalaman</p>
   </div>
 </div>

</div>

</div>
</div>
</div>
</div>

<div class="section-3">
 <div class="container-fluid">
  <div class="row justify-content-center">
    <div class="col-sm-12">
     <img src="{{url('vendor/assets/landing/icon/ic_sparation.png')}}" class="center-img"  width="auto" height="auto" alt="">
   </div>
   <div class="col-sm-12">
     <h5 class="title-section">Profil Konsultan Kami</h5>
   </div>
 </div>
 <div class="section-3-img">
  <div class="row">
    <div class="col-sm-6 foto">
      <div class="foto-content">
       <!--  <img src="{{url('vendor/assets/landing/image/putra png.png')}}" class="img-circle center-c" style="width: 85%;height: 100%" alt=""> -->
        <img src="{{url('vendor/assets/ddd.jpg')}}" class="img-circle center-c" style="width: 75%;height: 100%" alt="">
      </div>
    </div>
    <div class="col-sm-6" id="wow" style="height: 250px;margin-top: 285px;">
        <p class="head-coursel">Budy Santoso Santoso, SE, SH, BKP</p>
        <p class="content-coursel">with the permission of Tax Consultants since 2009 with permission certified Brevet C Number: KIP-3279 / IP.C / PJ / 2016 and licensed as a legal advisor at the tax court numbers KEP-137 / PP / IKH / 2016. We are members of the Association of Indonesian Tax Consultant licensed.</p>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <div style="text-align:center;">
      <a href="" class="tombol-coursel btn btn-sm btn-warning hvr-float-shadow"><span class="content-tombol">Lihat konsultan pajak lainya</span></a>
    </div>
  </div>
  </div>
  </div>

</div>
</div>


<div class="section-4" id="faq" style="margin-right: 0px!important;margin-left: 0px!important">
  <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-sm-12">
       <img src="{{url('vendor/assets/landing/icon/ic_sparation.png')}}" class="center" style="margin-bottom: 30px"  width="auto" height="auto" alt="">
     </div>
   </div>
   <div class="section-4-bg center">
    <div class="row">
      <div class="col-sm-6">
        <p class="font-section-4-head font-up">Anda membutuhkan informasi lebih detail? Silakan kontak kami</p>
      </div>
      <div class="col-sm-6">
        <div class="form-contact">
          <div class="form-contact-space">
            <p class="silakan-berbicara-de">
              Silakan berbicara dengan kami jika Anda memiliki pertanyaan. Kami berusaha menjawab dalam 24 jam.
            </p>
            <br>
          <form action="{{url('email')}}" method="post">
            {{csrf_field()}}
            <div class="form-group">
              <input type="text" name="nama" required="" class="form-control contact" placeholder="Nama">
            </div>
            <div class="form-group">
              <input type="email" name="email" required="" class="form-control contact" placeholder="Email">
            </div>
            <div class="form-group">
              <input type="text" name="pesan" required="" class="form-control contact-pesan" placeholder="Pesan">
            </div>
            <br>
            <div class="box-footer">
              <button type="submit" class="btn btn-sm rectangle-copy hvr-float-shadow"><span class="kirim-pesan">KIRIM PESAN</span></button>
            </div>
          </form>
          </div>
        </div>
        
      </div>
    </div>
  </div>
<!--   <a class="btn btn-sm btn-primary hvr-float-shadow pull-right" style="margin-top: 60px;" data-easing="easeInQuad" href="#pajak"><i style="font-size: 30px" class="fa fa-arrow-circle-up" aria-hidden="true""></i><div>back top</div></a> -->
</div>
</div>

<footer class="footer">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <hr class="hr-foot">
        <p class="font-foot">© Copyright Konsultasi Pajak Online 2019</p>

      </div>
    </div>
  </div>
</footer>


@endsection

@push('script')
<script>

 $(document).ready(function(){
  $('.carousel').carousel({
    interval: 2000
  })
  $("#img1").mouseover(function(){
    $("#g1").attr("src", "{{url('vendor/assets/landing/icon/ic_law_white@2x.png')}}");
    $("#f1").css("color","white");
    $("#f11").css("color","white");

  });
  $("#img1").mouseout(function(){
    $("#g1").attr("src", "{{url('vendor/assets/landing/icon/ic_law@2x.png')}}");
    $("#f1").css("color","black");
    $("#f11").css("color","black");
  });

  $("#img2").mouseover(function(){
    $("#g2").attr("src", "{{url('vendor/assets/landing/icon/ic_tax_audit_white@2x.png')}}");
    $("#f2").css("color","white");
    $("#f22").css("color","white");
  });
  $("#img2").mouseout(function(){
    $("#g2").attr("src", "{{url('vendor/assets/landing/icon/ic_tax_audit@2x.png')}}");
    $("#f2").css("color","black");
    $("#f22").css("color","black");
  });

  $("#img3").mouseover(function(){
    $("#g3").attr("src", "{{url('vendor/assets/landing/icon/ic_licence_white@2x.png')}}");
    $("#f3").css("color","white");
    $("#f33").css("color","white")
  });
  $("#img3").mouseout(function(){
    $("#g3").attr("src", "{{url('vendor/assets/landing/icon/ic_licence@2x.png')}}");
    $("#f3").css("color","black");
    $("#f33").css("color","black");
  });
});


    var scroll = new SmoothScroll('a[href*="#"]:not([data-easing])');

      if (document.querySelector('[data-easing]')) {

        var easeInQuad = new SmoothScroll('[data-easing="easeInQuad"]', {easing: 'easeInQuad'});
      }
        
</script>

<!-- <blockquote class="embedly-card"><h4><a href="https://tawk.to/chat/5c678dbaf324050cfe33795c/default">null</a></h4><p>null</p></blockquote>
<script async src="//cdn.embedly.com/widgets/platform.js" charset="UTF-8"></script> -->

@endpush
